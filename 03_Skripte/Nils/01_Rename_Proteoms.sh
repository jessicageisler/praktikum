# Zuerst einen Ordner "all_Databases" erstellen
# dieses Skript baut auf "proteomdownload.py" auf
# es ist notwendig, dass der Dateipfad in jeder Zeile zum Ordner "Proteom" führt!!!
# -->> search & replace mit dem Editor

cd ./all_Databases/
cp  /home/fenrir/Dokumente/bioinformatik/praktikum/Proteom/Bilateria/GCF_000001215.4_Release_6_plus_ISO1_MT_protein.faa DMEL
cp  /home/fenrir/Dokumente/bioinformatik/praktikum/Proteom/Bilateria/Hofstenia_miamia.HmiaM1.pep.all.fa HMIA
cp  /home/fenrir/Dokumente/bioinformatik/praktikum/Proteom/Choanoflagellata/01_Diaphanoeca_grandis.proteins.fasta DGRA
cp  /home/fenrir/Dokumente/bioinformatik/praktikum/Proteom/Choanoflagellata/02_Acanthoeca_spectabilis.proteins.fasta ASPE
cp  /home/fenrir/Dokumente/bioinformatik/praktikum/Proteom/Choanoflagellata/03_Salpingoeca_punica.proteins.fasta SPUN
cp  /home/fenrir/Dokumente/bioinformatik/praktikum/Proteom/Choanoflagellata/04_Salpingoeca_urceolata.proteins.fasta SURC
cp  /home/fenrir/Dokumente/bioinformatik/praktikum/Proteom/Choanoflagellata/05_Hartaetosiga_gracilis.proteins.fasta HGRA
cp  /home/fenrir/Dokumente/bioinformatik/praktikum/Proteom/Choanoflagellata/06_Salpingoeca_macrocollata.proteins.fasta SMAC
cp  /home/fenrir/Dokumente/bioinformatik/praktikum/Proteom/Choanoflagellata/07_Stephanoeca_diplocostata.Australia.proteins.fasta SDIP.australia
cp  /home/fenrir/Dokumente/bioinformatik/praktikum/Proteom/Choanoflagellata/07_Stephanoeca_diplocostata.France.proteins.fasta SDIP.france
cp  /home/fenrir/Dokumente/bioinformatik/praktikum/Proteom/Choanoflagellata/08_Helgoeca_nana.proteins.fasta HNAN
cp  /home/fenrir/Dokumente/bioinformatik/praktikum/Proteom/Choanoflagellata/09_Salpingoeca_kvevrii.proteins.fasta SKVE
cp  /home/fenrir/Dokumente/bioinformatik/praktikum/Proteom/Choanoflagellata/10_Didymoeca_costata.proteins.fasta DCOS
cp  /home/fenrir/Dokumente/bioinformatik/praktikum/Proteom/Choanoflagellata/11_Choanoeca_perplexa.proteins.fasta CPEX
cp  /home/fenrir/Dokumente/bioinformatik/praktikum/Proteom/Choanoflagellata/12_Salpingoeca_infusionum.proteins.fasta SINF
cp  /home/fenrir/Dokumente/bioinformatik/praktikum/Proteom/Choanoflagellata/13_Microstomoeca_roanoka.proteins.fasta MROA
cp  /home/fenrir/Dokumente/bioinformatik/praktikum/Proteom/Choanoflagellata/14_Savillea_parva.proteins.fasta SPAR
cp  /home/fenrir/Dokumente/bioinformatik/praktikum/Proteom/Choanoflagellata/15_Hartaetosiga_balthica.proteins.fasta HBAL
cp  /home/fenrir/Dokumente/bioinformatik/praktikum/Proteom/Choanoflagellata/16_Salpingoeca_dolichothecata.proteins.fasta SDOL
cp  /home/fenrir/Dokumente/bioinformatik/praktikum/Proteom/Choanoflagellata/17_Codosiga_hollandica.proteins.fasta CHOL
cp  /home/fenrir/Dokumente/bioinformatik/praktikum/Proteom/Choanoflagellata/18_Salpingoeca_helianthica.proteins.fasta SHEL
cp  /home/fenrir/Dokumente/bioinformatik/praktikum/Proteom/Choanoflagellata/19_Mylnosiga_fluctuans.proteins.fasta MFLU
cp  /home/fenrir/Dokumente/bioinformatik/praktikum/Proteom/Choanoflagellata/GCF_000002865.3_V1.0_protein.faa MBRE
cp  /home/fenrir/Dokumente/bioinformatik/praktikum/Proteom/Choanoflagellata/GCF_000188695.1_Proterospongia_sp_ATCC50818_protein.faa SROS
cp  /home/fenrir/Dokumente/bioinformatik/praktikum/Proteom/Cnidaria/GCA_000827895.1_ASM82789v1_protein.faa TKIT
cp  /home/fenrir/Dokumente/bioinformatik/praktikum/Proteom/Cnidaria/GCA_010108815.1_TAU_Msqu_1_protein.faa MSQU
cp  /home/fenrir/Dokumente/bioinformatik/praktikum/Proteom/Cnidaria/GCF_000004095.1_Hydra_RP_1.0_protein.faa HVUL
cp  /home/fenrir/Dokumente/bioinformatik/praktikum/Proteom/Cnidaria/GCF_000209225.1_ASM20922v1_protein.faa NVEC
cp  /home/fenrir/Dokumente/bioinformatik/praktikum/Proteom/Cnidaria/GCF_001417965.1_Aiptasia_genome_1.1_protein.faa EPAL
cp  /home/fenrir/Dokumente/bioinformatik/praktikum/Proteom/Cnidaria/GCF_002042975.1_ofav_dov_v1_protein.faa OFAV
cp  /home/fenrir/Dokumente/bioinformatik/praktikum/Proteom/Cnidaria/GCF_002571385.1_Stylophora_pistillata_v1_protein.faa SPIS
cp  /home/fenrir/Dokumente/bioinformatik/praktikum/Proteom/Cnidaria/GCF_003704095.1_ASM370409v1_protein.faa PDAM
cp  /home/fenrir/Dokumente/bioinformatik/praktikum/Proteom/Cnidaria/GCF_004143615.1_amil_sf_1.1_protein.faa AMIL
cp  /home/fenrir/Dokumente/bioinformatik/praktikum/Proteom/Cnidaria/GCF_004324835.1_DenGig_1.0_protein.faa DGIG
cp  /home/fenrir/Dokumente/bioinformatik/praktikum/Proteom/Cnidaria/GCF_009602425.1_ASM960242v1_protein.faa ATEN
cp  /home/fenrir/Dokumente/bioinformatik/praktikum/Proteom/Ctenophora/Mnemiopsis_leidyi.MneLei_Aug2011.pep.all.fa MLEI
cp  /home/fenrir/Dokumente/bioinformatik/praktikum/Proteom/Filasterea/GCF_000151315.2_C_owczarzaki_V2_protein.faa COWC
cp  /home/fenrir/Dokumente/bioinformatik/praktikum/Proteom/Fungi/GCF_000146045.2_R64_protein.faa SCER
cp  /home/fenrir/Dokumente/bioinformatik/praktikum/Proteom/Fungi/GCF_000182925.2_NC12_protein.faa NCRA
cp  /home/fenrir/Dokumente/bioinformatik/praktikum/Proteom/Placozoa/GCA_003344405.1_TrispH2_1.0_protein.faa TSPH
cp  /home/fenrir/Dokumente/bioinformatik/praktikum/Proteom/Placozoa/GCF_000150275.1_v1.0_protein.faa TADH
cp  /home/fenrir/Dokumente/bioinformatik/praktikum/Proteom/Porifera/EMUE_T-PEP_130911 EMUE
cp  /home/fenrir/Dokumente/bioinformatik/praktikum/Proteom/Porifera/GCF_000090795.1_v1.0_protein.faa AQUE
cp  /home/fenrir/Dokumente/bioinformatik/praktikum/Proteom/Porifera/OCAR_T-PEP_130911 OCAR
cp  /home/fenrir/Dokumente/bioinformatik/praktikum/Proteom/Porifera/SCAR_T-PEP_160304 SCAR
cp  /home/fenrir/Dokumente/bioinformatik/praktikum/Proteom/Porifera/SCIL_P-CDS_130802 SCIL
cp  /home/fenrir/Dokumente/bioinformatik/praktikum/Proteom/Porifera/XTES_T-PEP_160304 XTES
cp  /home/fenrir/Dokumente/bioinformatik/praktikum/Proteom/Teretosporea/GCF_001186125.1_Spha_arctica_JP610_V1_protein.faa SARC
cp  /home/fenrir/Dokumente/bioinformatik/praktikum/Proteom/Viridiplantae/GCF_000001735.4_TAIR10.1_protein.faa ATHA
