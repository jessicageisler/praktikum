#!/bin/bash


#Purpose: converts tab-separated txt-files to comma-separated csv files 
#Input: tab-separted table with filtered and sorted blastp results (generated using script '5_joining_family_blastscores') 
#Output: comma-separted csv file with filtered and sorted blastp results
#script must be run from the folder containing all the input files 
#input directory: ./Proteome/allDB/blast/summary/comb
#output directory: ./Proteome/allDB/blast/summary/comb


for file in *.txt
	do
		nvar=$( echo $file | sed -e  's/.txt//g' )
		cat $file | tr "\\t" "," > ${nvar}.csv
	done
