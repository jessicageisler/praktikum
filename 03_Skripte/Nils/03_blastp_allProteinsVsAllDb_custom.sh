#!/bin/sh
# Erstellt von allen Proteinen gegen die Proteome aller Spezies 
# die Blast-Tabellen mit folgendem Header (durch Tabulator getrennt):
#
# Taxa, sseqid, evalue, qcovs, score, bitscore, pident, length, mismatch, gapopen, qstart qend sstart send
# Lesen von 2 Listen um die Dateien abzuarbeiten
	# dblist.txt
	# proteinlist.txt
# Ausgabe in Ordner "CUSTOM"


while read protein
	do 
	while read db
		do
		dbx=$( echo $db)
		blastp -db ~/Dokumente/bioinformatik/praktikum/all_Proteoms/$db -query ~/Dokumente/bioinformatik/praktikum/AASeqs/$protein -outfmt "6 sseqid evalue qcovs score bitscore pident length mismatch gapopen qstart qend sstart send" |  sed -e s/^/${db}"\t"/ > ~/Dokumente/bioinformatik/praktikum/blast/CUSTOM/${protein}_${db}_custom.txt
		done < ./dblist.txt
	
	blastp -db ~/Dokumente/bioinformatik/praktikum/all_Proteoms/$db -query ~/Dokumente/bioinformatik/praktikum/AASeqs/$protein -outfmt "6 sseqid evalue qcovs score bitscore pident length mismatch gapopen qstart qend sstart send" | sed -e s/^/${db}"\t"/ > ~/Dokumente/bioinformatik/praktikum/blast/CUSTOM/${protein}_${db}_custom.txt
	done < ./proteinlist.txt
