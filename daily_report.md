Bitte schreibt in diese Datei am Ende des Tages was ihr alles gemacht habt, einerseits für euch, damit damit ihr ein überblich habt eas ihr alles geschafft habt, andererseits für die Tutoren, damit wir wissen wo ihr jeweils grade steht.

Diese Datei soll also am Ende jeden Tages committed und gepushed werden.

In dem report sollten stehen: 
* ein Datum
* Erkenntnisse und was ihr gelernt habt, insbesondere über eure Proteine.
* Verweise auf Graphiken und Bäume die ihr erstellt habt.
* Offene Probleme sowie  Aufgaben und Ideen wie ihr damit weitermachen werdet.
* aus dem protoll darf gerne hervorgehen wer an was gearbeitet hat, insbesondere dann, wenn ihr das arbeitsergebnis verschiedener Personen  über einen account committed. 



**Tag 1: 22.06.2020**
- Erste Schritte mit Git gelernt (alle)
- Aminosäurensequenzen der Proteine der Gruppe in Datenbank gesucht + Blast Recherche (Jessica)
- Recherchieren über die einzelnen Proteine und Zusammenfassung (Nils, Jenny)
- Python skript schreiben um die Proteome geordnet runterzuladen und abzuspeichern (Iris)

**Tag 2: 23.06.2020**
- Runterladen der Proteomdaten mit dem Skript
- Einzelne Dateien mussten hier noch gesondert behandelt werden, da der Link falsch war oder die Datei nicht entpackt werden konnte etc. 
- Erstellen der DB mit Blast. Hierbei gab es Probleme hinsichtlich der Frage, ob es eine gesamte DB sein soll und wie dies anzustellen ist 
    oder ob für jeden File eine eigene DB entstehen soll. Das hat viel Zeit in Anspruch genommen.
    Antwort: die DB müssen einzeln für jede Spezies erstellt werden um den besten Hit je Spezies finden zu können.
- Außerdem gab es Verwirrungen über eines unserer Proteine. Da der Name nirgendwo zu finden war. Es handelt sich scheinbar aber um ein Synonym.
- Blasten der erstellten .fasta-files unserer Proteine gegen die von uns erstellte DB. Fehlermeldung "BLAST Database error: No alias or index file found for protein database xy.faa", noch nicht gelöst (Jessica)
- 
**Tag 3: 24.06.2020**
- Problem mit DB gelöst, Datenbank-Format vermutlich falsch; nun mit neuem Befehl neu  (Nils)
- Ausführen im "Proteom"-Ordner in dem die Proteome heruntergeladenen wurden:

$   for file in */*; do makeblastdb -in $file -dbtype prot; done

- oder einzeln einfach:

$   makeblastdb -in proteomfastadatei.faa -dbtype prot

- Erstellen von Fasta-Dateien aus den Aminosäurensequenzen (NCBI, .. ) der Proteine (Jessica)

$   echo "sequenz in fastaformat" >> sequenz.fasta
- **Blasten: ** (aufgeteilt unter Jessica, Iris und Nils)

- Nun mit richtiger Datenbank: 
$ blastp -db proteomfastadatei.faa -query proteinsequenz.fasta > proteinvsproteom.txt 

- Ordner erstellt "BlastedProteins" und Einordnung der Verschiedenen Blast-Textfiles zu den einelnen Taxa (Jessica)
- Einfügen der Blastfiles in die einzelnen Ordner (alle); Überlegungen zur Weiterverarbeitung der Blastfiles in tabellarischer Form; erneut Blasten um direkt tabellarische Form zu erhalten (Jenny)
- - Bedeutung der einzelnen Scores/Values recherchiert (nils)

- BigBlueButton leider seit ca. 16:45 down. Weiterarbeiten nicht möglich. (Jessica)

nächtlicher NACHTRAG:
Automatisiertes Blasten und Ausgabe der Blast-Datei in Tabellenform (Jenny)
    
$ dbquery.sh:
    
    #!/bin/sh

    while read protein
    	do 
    	while read db
    		do
    		blastp -db $db -query $protein -outfmt 6 > ./blast/${protein}_${db}.txt
    		done < ./dblist.txt
	
    	blastp -db $db -query $protein -outfmt 6 > ./blast/${protein}_${db}.txt
    	done < ./proteinlist.txt

- Vorbereitung: Umbennung der entzippten Proteom-Fastas entsprechend der vorgegebenen Abkürzung in proteomeTable.txt (ABCD, in Grossbuchstaben!, ohne ".fa, .faa, .fasta"), 
        dann Erstellung der zugehörigen DB files mit  $ for file in *; do makeblastdb -in $file -dbtype prot; done
- Erstellung der Liste der Proteinquery-Dateien (proteinlist.txt) und der DB-Liste (dblist.txt)
- Sammlung aller DB.fa und proteinquery.fa -Dateien sowie von proteinlist.txt und dblist.txt in einem Ordner; Erstellung eines Unterordners blast für die Ergebnisse
- aus diesem Sammelordner heraus: Blasten mit 
        $ bash dbquery.sh
    
- automatisiertes Blasten funktioniert, aber die tabellerischen Dateien wurden noch nicht in git gepusht, da aufgrund von zu niedriger Übertragungsgeschwindigkeit beim Download noch einzelne DBs fehlen (kamen nicht beim automatischen Download mit, manueller Download notwendig)
- Erstellung von Entwürfen zum Filtern und Sortieren der nicht-tabellarischen (ohne -outfmt 6) und der tabellarischen Blast.output Dateien basierend auf awk (Jenny)

**Tag 4: 25.06.2020**

- wir haben scheinbar etwas zu viel Zeit damit verbracht, die einzelnen Proteine gegen die einzelnen Datenbanken zu alignen und in einzelnen Ordner zu packen. Außerdem war die Ausgabe nicht so, wie wir wollten (in nützlicher Tabellenform). Das müssen wir heute nochmal schnell automatisiert nachholen. (Jessica)

- Proteomdaten in Fasta umbenennen, wie in Tag 3 angegeben. Erstellen der DB Files wie ebenfalls aus Tag 3 zu entnehmen. (alle)
- Anpassung des Skripts fuer das Blasten - Output einmal in Tabellenform (ohne Alignments), einmal nur das Alignment (in Arbeit)
    Gespräch mit den BetreuerInnen darüber, dass wir das Alignment nicht brauchen. Wir suchen lediglich nach einen oder mehreren guten Hits in der
    Spezies für jedes Protein. Damit wollen wir nachvollziehen in welchen Spezies und phylogenetischen Branches wir das jeweilige Protein finden können.
    
**Tag 5: 26.06.2020**

- Auswertung der erstellten Blasttabellen. Dafür wird ein Skript geschrieben, welches bereits die besten Hits auswählt und zusammen in ein File fügt.
    Damit sollen die Ergebnisse übersichtlicher dargestellt werden und leichter ausgewertet werden können. Es soll uns ersparen alle Datein einzeln
    anschauen zu müssen. (Jenny)
    
- Beschäftigung mit Splitstree um die Ergbenisse im Folgenden darstellen zu können.
    Aus den ausgewählten Hits beim Blast soll pro Protein ein Tree erstellt werden der zeigt in welchen Spezies das Protein gefunden wurde. 
    Erkenntnis, dass SplitsTree aus einer Multiplen Sequenzalignment im fasta Format einen Baum generieren kann. Vorher musste SplitsTree zum 
    Laufen gebracht werden. Die Version SplitsTree 4 ist auf Windows mit Bugs behaftet. Der Fehler konnte nicht behoben werden. Mit der Version 
    SplitsTree 5 taucht er jedoch nicht mehr auf. (Iris)

- Testweise händisches Auswerten der Blasts für das Protein SCMH1 mit den verschiedenen Spezies. Vorerst wurden die Hits mit den besten e-value
    recht grob aussortiert. Dabei wurde beachtet, dass die Alignments über mehr als 70 AS gehen und eine relativ gute (>30%) Coverage besitzen. 
    Anschließend wurden testweise die original AS Sequenz des SCMH1 mit den ersten 17 Proteinen aus verschiedenen Spezies, welche einen Hit generierten
    über Clustalo alignt. (Iris)

$   clustalo -i "SCMH1_Multifasta.fasta" > "SCMH1_MSA.fasta" 

- Beim Betrachten des Alignment mit AliView konnte eine konservierte Region ausgemacht werden. Testweise wurde das Alignment mit SplitsTree geöffnet. 
    Es konnte ein Baum erstellt werden, der auf den ersten Blick allerdings nicht besonders aussagekräftig scheint. 
    Problematisch ist die Bezeichnung der einzelnen Blätter im Baum. Diese tragen im Moment die komplette Sequenzbezeichnung aus dem Aligment für 
    jede Sequenz. Das muss zur besseren Übersicht geändert werden. 
    Das zum Testen erstellte Multifasta und MSA kann unte dem Order Auswertung.Blast.Test gefunden werden. (Iris)

NACHTRAG: (Jenny)
- die Skripts zum automatischen Filter und Sorten zur einfachereren Auswertung der BLAST-Ergebnisse wurde erfolgreich erstellt;
   aus technischen Gruenden koennen die finalen zusammengefassten Ergebnisdateien erst am Montag ins Git geladen werden; bis auf das letzte Skript zum Joinen sind die Skripte hochgeladen
- Folgende Schritte wurden vorgenommen (alles automatisiert) (Jenny):
   - Filter auf evalue < 0.00001 und sortieren nach evalue (aufsteigend)
   - die 5 Matches mit dem niedrigsten evalue fuer jede Query wurden in einer Datei zusammengefasst, d.h. pro query Protein gibt es ein File mit den Treffern gegen alle DBs
   - Joinen der Tabelle mit den query Ergebnissen mit den Familiennamen der Proteine; 
   - in der joint table sind die Daten erstrangig nach "family", bei Gleichheit dieser nach "ssequid" und bei Gleichheit dieser nach "evalue" sortiert
   - für Spezien bzw. DBs für die kein signifikanter Match mit dem entsprechenden Protein gefunden wurde steht NA; 
      auf dieser Weise lässt sich schnell sehen, ob für das Protein in dieser Spezies ueberhaupt ein signifikanter Match gefunden wurde
    - Auswertung der Tabellen erfolgt im Laufe des Wochenende bzw. am Montag

   - die Tabelle enthält folgende Spalten (mit NA, wenn kein Match mit evalue < 0.00001 vorliegt): 
     family, species_abbrevm, species_name, qseqid, sseqid, evalue, bitscore, pident, length, mismatch, gapopen, qstart, qend, sstart, send

NACHTRAG: (Nils)
- Extrahieren der Sequenzen aller Hits aus den Datenbanken der Proteome
- hierzu wurden zunächst die Hits wie folgt gewählt:
         kleinster e-value und der höchste Coverage

$   for f in PHC1seq_*_tab.txt; do cat ${f} | sort -k11g -rk3 | head -1l | cut -f2 >> PHC1seq_allHitIDs ; done;
    
- Die Sequenzen für das jeweilige Protein wurden mit folgendem Befehl aus den Datenbanken extrahiert
    
$   for file in ./all_Proteoms/*
    do
    fastacmd -d $file -i ~/.../All_Hits/PHC1seq_allHitIDs >> ./sequencesOfHits/PHC1_Hits-Sequences.fasta;
    done

- Ziel dabei war es mit diesen Sequenzen nun Multiple Alignments zu erstellen

**Tag 6: 29.06.2020**

- Problemevaluation & Lösung (alle): Die Tabellen mit den 5 besten Hits pro Protein für jede Spezies sowie der Abgleich dieser Daten mit der Hypothese liegt vor (Ordner Auswertung.Blast.Tast/table). 
    Diese Tabellen wurden mit den Skripten 2-6 (im Order 'Code') erstellt. Zu großem Teil entsprechen die Daten der Hypothese. Weitere Zusammenfassung & Interpretation dieses Abgleichs ist in Arbeit.(Jenny)
- Ebenfalls vor liegen die Multifasta Dateien, die Nils erstellt hat. Darin stehen die AS-Sequenzen der Hits für jedes unserer Query Proteine. Problematisch ist, dass 
    in den Mutlifasta die header nur teilweise die korrekte Bezeichnung des gefundenen Proteins und der Spezies enthalten. Dadurch ist ein späteres
    Zuordnen kaum möglich. So geht die genaue Zuordung auch für die weiteren Schritte, also das Multiple Sequenzalignment (MSA) und den
    Tree verloren. Die Zuordnungen sind unbedingt notwendig, um überhaupt Informationen in dem Tree darstellen zu können. 
    Um dies zu lösen wird im Folgenden versucht bei der Erzeugung des Mutlifasta mit den AS-Sequenzen ein Teil des Filenamens zu übernehmen. 
    Dieser enthält jeweils die genaue Bezeichnug für die Spezies. Außerdem soll der Name des Phylum übernommen werden, um die spätere Sortierung
    im Tree zu erleichtern. Dazu kann ein Teil des Codes von Jenny genutzt werden. 
    Außerdem muss in den Multifasta Dateien noch die originale AS-Sequenz unseres Query Proteins für das anschließende MSA eingefügt werden. 
    Da diese als Referenzsequenz benutzt werden soll. 
    Anschließend wird aus dem MSA jeweils mit TreesSplit ein Baum erstellt
- Unstimmigkeiten bei Matches zwischen den Ergebnissen in Tabellenform und jenen für das MSA. Fehlersuche läuft. 
    Einigung darauf einen evalue Schwellwert von 10^-5 zu verwenden, basierend auf der Publikation von Richter et al. 2018. Sortierung in erste Instanz nach dem evalue,  
    bei Gleichheit dessen in 2.Instanz nach pident, um die Coverage zu beruecksichtigen wie bereits von Nils vorgenommen worden war. Die Coverage wird bei Richter et al. nicht berücksichtigt.
- Beim Versuch sich direkt die coverage (qcov) beim Blasten ausgeben zu lassen resultierte in nicht loesbarem Error.
  Command:  blastp -db $db -query $protein -outfmt "6 qseqid sseqid evalue bitscore pident length qcovs"  > ./blast/${protein}_${db}_tab.txt  done < ./dblist.txt
  Error: Too many positional arguments (1), the offending value: ...
  Daher Verwendung von pident aus der default Ausgabe, um daraus Rueckschluesse auf die coverage des query proteins zu ziehen.

- Beantwortung der Fragen aus den Aufgabenstellungen, Recherche zur Theorie hinter den Aufgabenstellungen (Jessica, Iris)

NACHTRAG: (Nils)

Hits von Tag 6 nun mit der zugehörigen Taxa in einer Datei. Leider stehen Taxa und Hit-Sequence-ID untereinander, d.h.:
    - 1. Zeile: Taxa
    - 2. Zeile zugehörige Hit-Sequenz-ID
    Code-Beispiel wurde für jedes Protein auf alle Datenbanken angewendet:
    
    $   for f in PHC1seq_*_tab.txt; do echo $f  | sed -e 's/PHC1seq_//g' | sed -e 's/_tab.txt//g' >> TaxaAndHitID_PHC1; cat ${f} | sort -k11g -rk3 | head -1l | cut -f2 >> TaxaAndHitID_PHC1 ; done;
    
    NACHTRAG: vermutlich Lösung für dieses Problem gefunden. Verwendung von awk, ähnlich wie im Skript (4_blastsummary)
    
**Tag 7: 30.06.2020**

- Beantwortung weiterer Fragen aus dem Fragenkatalog (Jessica, Iris)
- Probleme mit dem Blast und dem Auswählen der Hits wurde erkannt. Der Filter auf den Evalue war zu lasch angesetzt. Es wird erneut geblastet,
    da wir uns beim Blasten nicht alle Werte, die wir brauchen haben ausgeben lassen. Daher wird neu geblastet und das Skript leicht abgeändert.
- Die neu erstellten Sequenzlisten der Hits vom Blast wurden nochmal händisch überarbeitet. Die Proteine kommen teilweise mehrfach und in unterschiedlicher Länge vor. 
    Es wurde darauf geachtet stehts die längste Sequenz zu behalten. Außerdem wurden die header als Namen der Sequenzen so überarbeitet, dass im MSA
    einfach die Spezies, das Phylum und das Protein abgelesen werden kann. 
- Die MSA für PHC1-3 und RING1 wurden über clustal erstellt. 

$ clustalo -i Inputdatei.fasta > Outputdatei.fasta

- Die MSA wurden anschließend über SplitsTree als UPGMA Tree dargestellt. Dabei traten zahlreiche Fehler auf. Für RING1 konnte nich kein Tree erstellt werden, 
    da es einen Fehler in der Distanzmatrix gäbe, durch einen falschen Buchstaben "m". Leider konnte dies durch Anschauen des MSA im AliView nicht bestätigt werden. 
    Der Fehler konnte leider noch nicht gelöst werden. Außerdem gab es einige Probleme mit SplitsTree, das Programm ließ sich eine Weile nicht öffnen. 
    Leider wurde die Beschriftung von den Sequenzen auch nicht übernommen, so dass die einzelnen Branches nochmal händisch beschriftet werden mussten. 
    Die Trees wurden ein mal als Bild exportiert und außerdem als SplitsTree Datei abgespeichert. 
- Beim Anschauen der bisher erstellten MSA konnten zahlreiche gut alignte Bereiche ausgemacht werden. Es konnte jedoch noch nicht abgeglichen werden ob es sich dabei um Domänen handelt und welche dies sein könnten.
- Alle Trees und MSA die es bislang gibt können sich im Order MSA_Trees angeschaut werden. (MSA + Trees Iris erstellt)
- HMMER: wird benutzt, um in Sequenzdatenbanken nach homologen Sequenzen zu suchen. Wir verwenden hierfür die Sequenzen unserer Proteindomänen, wobei wir hier HM-Modelle aus der Pfam nehmen. Diese werden dann gegen unsere erstellten Proteindatenbanken "geHMMERt". Wichtig ist hierbei der Vergleich mit unseren vorigen Ergebnissen aus BLAST. Erstmal ist es wichtig, sich mit den Proteindomänen vertraut zu machen und zu schauen, was man bei dem Vergleich mit den Blastergebnissen erwartet. (Jessica) 
- Blast-Output indiviudell verändert: 
    - jetzt beinhalten die Tabellen den Datenbanknamen (= Spezies-Abkürzung) somit ist die Zuordnung der einzelnen Hits mit ihrer jeweiligen Sequenz-ID kein Problem mehr

Aus dem Skript 2_dbquery_neu.sh:

while read protein
        do 

        while read db
                do
                dbx=$( echo $db)

                blastp -db $db -query $protein -outfmt "6 sseqid evalue qcovs score bitscore pident length mismatch gapopen qstart qend sstart send" | sed -e s/^/${db}"\t"/  > ./blast/${protein}_${db}_tab.txt
                done < ./dblist.txt

        blastp -db $db -query $protein -outfmt "6 sseqid evalue qcovs score bitscore pident length mismatch gapopen qstart qend sstart send" | sed -e s/^/${db}"\t"/  > ./blast/${protein}_${db}_tab.txt
        done < ./proteinlist.txt

    
    -   sed -e s/^/ (setzt an den Anfang jeder Zeile der Datei )
    -   ${db} ist der Dateiname der Datenbank innerhalb der for-Schleife
    -   "\t" Tabulator
    -   / (gehört zu dem sed Befehl, um den eingefügten String abzugrenzen)
   
    -   nach der Vereinigung mit den Taxa Bezeichungen (Skript 5), enthaelt die Tabelle folgende Spalen: lastp_allProteinsVsAllDb_OutFmtCUSTOMTab.sh"
        Der Header ist hier:
            1.  Taxa 
            2   Spezies-Abkürzung
            3.  Spezies Name 
            4.  sseqid (Sequenz-ID)
            5.  evalue (Expect value)
            6.  qcovs (Query Coverage Per Subject)
            7.  score
            8.  bitscore
            9.  pident (Percentage of identical matches)
            10. length (Alignment length)
            11. mismatch
            12. gapopen
            13. qstart 
            14. qend
            15. sstart
            16. send
    

Die Daten wurden neu geblastet, diesmal mit zusaetzlicher Ausgabe des Wertes qcovs und score (raw score). Filtern auf evalue < 10^-10. 
diese Daten sind im Ordner Die Ergebnisse dafuer sind im Order Auswertung.blast.test/table/blast.
Geplant war das Filtern zusaetzlich auf qcovs > 60 und dann sortierung der gefilterten Daten nach dem Score. Diese befinden sich im Unterordner .../filtered_evalue_qcov_scoreSorted.
Aufgrund des relativ geringen Outputs wurden die Daten dann nur auf evalue < 10^-10 gefiltert, ohne Beruecksichtigung des qcovs. Diese daten befinden sich im Utnerordner .../filtered.

**Tag 8: 01.07.2020**

- HMMer: Skript zur Ausführung mit HMMer geschrieben mit E-Value cutoff von 10^-5; Liegt im Ordner Proteome/AlleDatenbanken/DomainHMM inklusive Anleitung, HMM-Files und Shell-Skript (geschrieben von Jessica). 
- Im Gespräch mit den Betreuer*innen wurden die Ergebnisse als Baum angesehen. Die Darstellung als UPGMA Tree eignet sich nicht. Dieser müsste 
   über Bootstrapping erst noch in seiner Struktur bestätigt werde. In der Darstellung als Split können auch teilweise widersprüchliche Informationen
    dargestellt werden. Der Tree benutzt nur die Informationen, welche als "überwiegend" interpretiert werden und vernachlässigt alles abweichende. 
    Im gemeinsamen Gespräch wurde der Split für PHC1 erläutert. Außerdem gab es den Auftrag die Hits und MSA für PHC1-3 gemeinsam in einem Split
    darzustellen. 


**Tag 9: 02.07.2020**

- (Iris) Betrachtet man die Splits für die Proteine PHC1-3 fällt sofort auf, dass wider Erwarten einige Choanoflagellata darin zu finden sind. 
    Den Cut-off für die Hits im Blast setzten wir auf 10^-10. Der Blast auf die Choanoflagellata SDOL erreichte für alle drei PCH Proteine
    einen Evalue der unter dem Cut-off lag. Außerdem haben sich noch SURC und MROA als Choanoflagellata in den Split verirrt. 
    Ihre Evalues lagen über dem Cut-off. Durch einen Fehler wurden sie trotzdem als Hit übernommen. SURC wurde für alle drei PHC Proteine übernommen. 
    Der Evalue von SURC liegt hierbei zwischen 1.02e-08 für PCH2 und 3.07e-09 für PCH1. MROA wurde nur für PHC1 übernommen. Der entsprechende Evalue liegt bei 5.02e-06.
    Anfangs untersuchten wir die Blast Ergebnisse mit einem Cut-off von 10^-5, wie in der Aufgabestellung vorgeschlagen. Wenn man diesen Cut-off setzt
    bekommt man weitere positive Treffer auf Choanoflagellata. Für das PHC1 Protein sind das CPEX, HNAN, SMAC und SROS. CPEX ergibt darüber hinaus
    auch einen Treffer im PHC2 Protein. Die Evalues liegen dabei im Bereich 10^-5. Da die Frage nach dem Vorkommen der PC1-3 Proteine innerhalb der Choanoflagellata von besonderem Interesse ist, 
    lohnt sich eine weitere Analyse. Es würde sich zum Beispiel anbieten die Proteine der Treffer in den Choanoflagellata einer genauen Analyse 
    hinsichtlich möglicher Domänen zu unterziehen. Durch den zeitlichen Rahmen des Praktikums war uns dies leider nicht mehr möglich. 

- In der Auswertung unserer HMM Domänensuche gegen unsere Hits aus dem Blast wurde für das Protein PHC1 die SAM Domäne für die Choanoflagellata 
    MROA (Protein m.157511) nachgewiesen. Interessant ist es auch deshalb, da sie nur durch einen Fehler überhaupt in die Hits gerutscht ist. Der Evalue
    für MROA im Blast war eigentlich über unserem Cut-off. In den anderen Choanoflagellata, teilweise mit Evalue <10^-10 konnte die SAM Domäne
    nicht gefunden werden. Eventuell ist das Alignment zwischen PHC1 und m.157511 von MROA nicht flächendeckens gut, aber im Bereich der Domäne
    doch vorhanden. Schon deshalb würde sich eine Suche nach SAM Domänen auch in den anderen Choanoflagellata, die einen ähnlichen Evalue wie MROA
    im Blast aufweisen lohnen. Die entsprechenden Kandidaten stehen im vorherigen Abschnitt (CPEX, HNAN, SMAC und SROS).

- Domänenabgleich SCMH1 (Iris)
Das SCMH1 Protein verfügt über vier verschiedene Domänen. In der HMM Analyse haben wir versucht diese Domänen jeweils in unseren Treffer Proteinen 
aus dem Blast zu finden. 
Die MBT Domäne, deren Funktion noch weitestgehend ungeklärt ist, konnte in 39 der 43 Sequezen unseres MSA der Blast Hits gefunden werden. 
Die Choanoflagellata SPAR und DGRA wiesen keine MBT Domäne auf. Ebenso wie die Porifera SCAR und OCAR. 
Die SLED Domäne, welche doppelsträngige DNA bindet, konnte nur bei einigen Spezies nachgewiesen werden. In allen Cnidaria, mit Ausnahme von Hydra vulgaris, konnte sie nachgewiesen werden. 
Auch in beiden Bilateria konnten wir die Domäne finden. Ebenso wie in einer Porifera (SCIL) und einer Placozoa (TSPH). 
Damit fehlt die zweite Placozoa, welche im MSA vorhanden ist, ebenso wir vier weitere Porifera. Auffällig ist, in den 22 Choanoflagellata aus dem MSA konnte keine SLED Domäne gefunden werden.
In der PFAM Datenbank kann der phylogenetische Tree der SLED Domäne angesehen werden. Darin zeigt sich, dass die SLED Domäne ausschließlich in Metazoa vorkommt, 
zu welchen die Choanoflagellata nicht zählen.  
Ähnlich verhält es sich mit der SAM Domäne. Auf PFAM lässt sich im phylogenetischen Tree ablesen, dass diese Domäne ausschließlich in Metazoa vorliegt. 
Wir finden sie durch die HMM Analyse in Bilateria, Cnidaria, Porifera und einem von beiden Placozoa. Auch hier wird sie wieder in der Placozoa TSPH gefunden. 
Aus dem phylogenetischen Tree lässt sich jedoch entnehmen, dass die Domäne ebenfalls in TSPH gefunden werden kann. Nur zeigen das unsere Daten nicht.
In übereinstimmung mit dem phylogenetischen Tree konnten wir keine SAM Domäne in unseren Proteinen der Choanoflagellata nachweisen.
Die RBR Domäne, welche eine RNA Bindestelle ist konnten wir per HMM Analyse ausschließlich in unserem Query Protein SCMH1 aus Homo sapiens nachweisen. 
Sie konnte ansonsten in keiner Spezies gefunden werden. Im phylogenetischen Tree zur RBR Domäne zeigt sich, dass die Domäne ausschließlich in Metazoa und da nur 
in Chordata auftaucht. Homo sapiens ist das einzige Chordata in unseren Daten.

- Insgesamt konnten wir nur die MBT Domäne in den meisten unserer Hits aus dem Blast finden. Bezüglich der Choanoflagellata kann davon ausgegangen werden, dass
die Proteine, welche wir im Blast mit SCMH1 gefunden haben nur wenig Ähnlichkeiten mit dem Query Protein aufweisen. Da nur eine von vier Domänen vorhanden ist, 
kann nicht davon ausgegangen werden, dass die Proteine die gleiche bzw. sehr ähnliche Aufgabe erfüllen. Als weiterführende Analyse könnten die Proteine aus Choanoflagellata
mit Pfam auf andere Domänen untersucht werden, welche eventuell ähnliche Funktionen übernehmen können. Da es sich bei Choanoflagellata um Einzeller handelt, kann ein
Fehlen der Domänen aus dem SCMH1 Protein mit der fehlenden Ausdifferenzierung der Zellen erklärt werden.

- Das betrachtete SCMH1 Protein ist ein Teil des PRC1 - Polycomb Repressive Complex 1, welcher im transkriptionellen Genesilencing durch Histonmodifikation eine Rolle spielt. 
In Schuttengruber, et al., 2017 ist nachzulesen, dass SCMH1 nur in Porifera, Placozoa, Cnidaria und Bilateria zu erwarten ist. 
Für Fungi, Filasterea, Teretosporea und Viridiplantea hatten wir wie erwartet bereits im Blast keine signifikanten Ergebnisse für den Vergleich mit SCHM1 erzielen können. 
Die in Cnidaria erzielten Treffer beinhalten Proteine, welche aus MBT Domänen bestehen, ebenso wie eine SLED Domäne und eine SAM Domäne (z.b. XP_020627343.1 aus OFAV).

    SLED Tree: https://pfam.xfam.org/family/PF12140.8#tabview=tab7
    Sam Tree: https://pfam.xfam.org/family/PF00536.30#tabview=tab7
    RBR Tree: https://pfam.xfam.org/family/PF17208.3#tabview=tab7
