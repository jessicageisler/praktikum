#!/bin/sh

while read hmm
	do 

	while read msa
		do

		hmmsearch -o ./results_notTable/hmm_${hmm}_${msa}.faa -E 1e-5 $hmm $msa
		done < ./protein_hits_multi_fa_list.txt
	
	hmmsearch -o ./results_notTable/hmm_${hmm}_${msa}.faa -E 1e-5 $hmm  $msa
	done < ./hmmlist.txt


while read hmm
	do 

	while read msa
                do

                hmmsearch --domtblout ./results_table/hmm_${hmm}_${msa}_table.faa -E 1e-5 $hmm $msa
                done < ./protein_hits_multi_fa_list.txt

       	hmmsearch --domtblout ./results_table/hmm_${hmm}_${msa}_table.faa -E 1e-5 $hmm  $msa
        done < ./hmmlist.txt



#hmmsearch --domtblout ./results/test4.faa -E 1e-5 SAM_1 PHC1_MSA.fasta
#--domtblout
