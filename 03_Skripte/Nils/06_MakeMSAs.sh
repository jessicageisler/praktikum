# Erstellt aus Multiple-Sequenz-Aligments mit Clustalo
# Als Input werden Sequenz-Sammlungen (Hits) in Form von .multifasta-Dateien benötigt.
# Ausgabe: "INPUTDATEINAME.msa"

cd SequencesOfHits/
for file in ./*
do
clustalo -i $file -o ~/Dokumente/bioinformatik/praktikum/MSA/${file}_evalue10-10.msa
done