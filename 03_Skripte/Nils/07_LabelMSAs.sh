sed -i 's/Acropora millepora/AMIL Cnidaria/' *.msa
sed -i 's/Amphimedon queenslandica/AQUE Porifera/' *.msa
sed -i 's/Dendronephthya gigantea/DGIG Cnidaria/' *.msa
sed -i 's/Drosophila melanogaster/DMEL Bilateria/' *.msa
sed -i 's/Exaiptasia pallida/EPAL Cnidaria/' *.msa
sed -i 's/Hydra vulgaris/HVUL Cnidaria/' *.msa
sed -i 's/Nematostella vectensis/NVEC Cnidaria/' *.msa
sed -i 's/Orbicella faveolata/OFAV Cnidaria/' *.msa
sed -i 's/Pocillopora damicornis/PDAM Cnidaria/' *.msa
sed -i 's/Stylophora pistillata/SPIS Cnidaria/' *.msa
sed -i 's/Trichoplax adhaerens/TADH Placozoa/' *.msa
sed -i 's/Trichoplax sp. H2/TSPH Placozoa/' *.msa
sed -i 's/Actinia tenebrosa/ATEN Cnidaria/' *.msa
sed -i 's/Capsaspora owczarzaki ATCC 30864/COWC Filasterea/' *.msa
sed -i 's/Capsaspora owczarzaki/COWC Filasterea/' *.msa
sed -i 's/Monosiga brevicollis MX1/MBRE Choanoflagellata/' *.msa
sed -i 's/Monosiga brevicollis/MBRE Choanoflagellata/' *.msa
sed -i 's/Salpingoeca rosetta/SROS Choanoflagellata/' *.msa
sed -i 's/ORF g.98980 m.98980/EMUE Porifera/' *.msa
sed -i 's/Amphimedon queenslandica/AQUE Porifera/' *.msa
sed -i 's/ORF g.112992 m.112992/ASPE Choanoflagellata/' *.msa
sed -i 's/ORF g.38431 m.38431/HBAL Choanoflagellata/' *.msa
sed -i 's/ORF g.59066 m.59066/HGRA Choanoflagellata/' *.msa
sed -i 's/ORF g.11924 m.11924/SDOL Choanoflagellata/' *.msa
sed -i 's/ORF g.82758 m.82758/SPUN Choanoflagellata/' *.msa
sed -i 's/ORF g.77554 m.77554/CPEX Choanoflagellata/' *.msa
sed -i 's/ORF g.48071 m.48071/DCOS Choanoflagellata/' *.msa
# Löscht in allen Zeilen mit dem Muster "type:" die Zeichen dahinter bis zum Ende der Zeile!
# inclusice "type:" ??? vielleicht auch nicht
sed -i 's/type:.*//g' *.msa
sed -i 's/gene:.*//g' *.msa
sed -i 's/AED:.*//g' *.msa
sed -i 's/; L(3)mbt.*//g' *.msa
sed -i 's/finger protein 1B-A;.*//g' *.msa
sed -i 's/ORF g.97624 m.97624/CHOL Choanoflagellata/' *.msa
sed -i 's/Capsaspora owczarzaki ATCC 3086/COWC Filasterea/' *.msa
sed -i 's/Capsaspora owczarzaki/COWC Filasterea/' *.msa
sed -i 's/ORF g.60952 m.60952/EMUE Porifera/' *.msa
sed -i 's/Dendronephthya gigantea/DGIG Cnidaria/' *.msa
sed -i 's/ORF g.144143 m.144143/DGRA Choanoflagellata/' *.msa
sed -i 's/Drosophila melanogaster/DMEL Bilateria/' *.msa
sed -i 's/ORF g.146848 m.146848/MROA Choanoflagellata/' *.msa
sed -i 's/COWC Filasterea4/COWC Filasterea/' *.msa
sed -i 's/Exaiptasia pallida/EPAL Cnidaria/' *.msa
sed -i 's/ORF g.21092 m.21092/OCAR Porifera/' *.msa
sed -i 's/ORF g.5426 m.5426/EMUE Porifera/' *.msa
sed -i 's/ORF g.61520 m.61520/SDOL Choanoflagellata/' *.msa
sed -i 's/pep scaffold:/HMIA Bilateria /' *.msa
sed -i 's/ORF g.311052 m.311052/OCAR Porifera/' *.msa
sed -i 's/-snap-gene-0.13-mRNA-1/ SCAR Porifera/' *.msa
sed -i 's/protein AED:0.11/XTES Porifera/' *.msa
sed -i 's/ORF g.49260 m.49260/\[SMAC Choanoflagellata\] /' *.msa 
sed -i 's/ORF g.117589 m.117589/\[SURC Choanoflagellata\] /' *.msa 
sed -i 's/ORF g.27466 m.27466/\[HNAN Choanoflagellata\] /' *.msa
sed -i 's/ORF g.49260 m.49260/\[SMAC Choanoflagellata\] /' *.msa 
sed -i 's/ORF g.37986 m.37986/\[MFLU Choanoflagellata\] /' *.msa 
sed -i 's/ORF g.789755 m.789755/\[SDIP.australia Choanoflagellata\] /' *.msa 
sed -i 's/ORF g.147770 m.147770/\[SKVE Choanoflagellata\] /' *.msa 
sed -i 's/ORF g.213393 m.213393/\[SPAR Choanoflagellata\] /' *.msa 
sed -i 's/ORF g.562951 m.562951/\[SDIP.france Choanoflagellata\] /' *.msa
sed -i 's/ORF g.61076 m.61076/\[SINF Choanoflagellata\] /' *.msa
sed -i 's/ORF g.133750 m.133750/\[SHEL Choanoflagellata\] /' *.msa 
sed -i 's/ORF g.148963 m.148963/\[SMAC Choanoflagellata\] /' *.msa
sed -i 's/ORF g.73694 m.73694/\[ASPE Choanoflagellata\] /' *.msa
sed -i 's/ORF g.104072 m.104072/\[DGRA Choanoflagellata\] /' *.msa
sed -i 's/ORF g.155329 m.155329/\[MROA Choanoflagellata\] /' *.msa
sed -i 's/ORF g.129792 m.129792/\[SPAR Choanoflagellata\] /' *.msa
sed -i 's/-snap-gene-0.39-mRNA-1 protein/ \[XTES Porifera\] /' *.msa
sed -i 's/RING2-A; RING/\[SCIL Porifera\] /g' *.msa 
sed -i 's/Arabidopsis thaliana/ATHA Viridiplantae/' *.msa 
sed -i 's/ORF g.214334 m.214334/ \[CHOL Choanoflagellata\]/' *.msa
sed -i 's/ORF g.176642 m.176642/\[SDOL Choanoflagellata\]/' *.msa
sed -i 's/ORF g.280421 m.280421/\[CPEX Choanoflagellata\] /' *.msa
sed -i 's/ORF g.35545 m.35545/\[SHEL Choanoflagellata\] /' *.msa 
sed -i 's/ORF g.11536 m.11536/\[DCOS Choanoflagellata\] /' *.msa
sed -i 's/ORF g.71937 m.71937/\[EMUE Porifera\] /' *.msa
sed -i 's/ORF g.38556 m.38556/\[MFLU Choanoflagellata\] /' *.msa
sed -i 's/ORF g.217319 m.217319/\[SMAC Choanoflagellata\] /' *.msa
sed -i 's/ORF g.68598 m.68598/\[HBAL Choanoflagellata\] /' *.msa
sed -i 's/ORF g.43935 m.43935/\[OCAR Porifera\] /' *.msa
sed -i 's/ORF g.185670 m.185670/\[HGRA Choanoflagellata\] /' *.msa
sed -i 's/ORF g.86770 m.86770/\[SURC Choanoflagellata\] /' *.msa 
sed -i 's/ORF g.300852 m.300852/\[HNAN Choanoflagellata\] /' *.msa 
sed -i 's/ORF g.61866 m.61866/\[SKVE Choanoflagellata\] /' *.msa
sed -i 's/ORF g.1043636 m.1043636/\[SDIP.australia Choanoflagellata\] /' *.msa 
sed -i 's/ORF g.1344514 m.1344514/\[SDIP.france Choanoflagellata\] /' *.msa 
sed -i 's/ORF g.207429 m.207429/\[SPUN Choanoflagellata\] /' *.msa 
sed -i 's/scgid3531/\[SCIL Porifera\] /' *.msa 
sed -i 's/; L(3)mbt.*//g' *.msa
sed -i 's/ORF g.43162 m.43162/\[ASPE Choanoflagellata\] /' *.msa
sed -i 's/ORF g.23454 m.23454/\[SPAR Choanoflagellata\] /' *.msa 
sed -i 's/ORF g.89146 m.89146/\[SDOL Choanoflagellata\] /' *.msa
sed -i 's/-0.12-mRNA-1 protein/\ [XTES Choanoflagellata\] /' *.msa 
sed -i 's/scgid4377/\[SCIL Porifera\] /' *.msa 
sed -i 's/scgid28052/\[SCIL Porifera\] /' *.msa 

# Ersetze den ">lcl|" durch ">(PHC1)" in der entsprechenden Datei
sed -i 's/>lcl|/\>(PHC1\) /' Hit_Sequences_PHC1.multifasta
sed -i 's/>lcl|/\>(PHC2\) /' Hit_Sequences_PHC2.multifasta
sed -i 's/>lcl|/\>(PHC3\) /' Hit_Sequences_PHC3.multifasta
sed -i 's/>lcl|/\>(RING1\) /' Hit_Sequences_RING1.multifasta_evalue10-10.msa
sed -i 's/>lcl|/\>(RING2\) /' Hit_Sequences_RING2.multifasta_evalue10-10.msa
sed -i 's/>lcl|/\>(RING2 Isoform\) /' Hit_Sequences_RING2isoform.multifasta_evalue10-10.msa
sed -i 's/>lcl|/\>(SCMH1\) /' Hit_Sequences_SCMH1.multifasta_evalue10-10.msa
sed -i 's/>lcl|/\>(SCML2\) /' Hit_Sequences_SCML2.multifasta_evalue10-10.msa
sed -i 's/>lcl|/\>(SCML2 Isoform\) /' Hit_Sequences_SCML2isoform.multifasta_evalue10-10.msa













