# In den Folgenden Multiplen-Sequenz-Alignments wurde "X" durch "*" ersetzt,
# da man es sonst nicht mit dem Programm SplitsTree verwenden kann!
# "*" steht in SplitsTree für fehlender Character


# In den Folgenden Multiplen-Sequzenz-Alignment wurde "X" durch "*" ersetzt,
# da man es sonst nicht mit dem Programm SplitsTree verwenden kann!


Anzahl	Zeile	Dateiname	Sequenz
2	867	RING1.msa	>(RING1) XP_020908555.1 E3 ubiquitin-protein ligase RING2 [EPAL Cnidaria]
	869
2	917	RING2.msa	>(RING2) XP_020908555.1 E3 ubiquitin-protein ligase RING2 [EPAL Cnidaria]
	918

2	877	RING12		>(RING1) XP_020908555.1 E3 ubiquitin-protein ligase RING2 [EPAL Cnidaria]
	879			>(RING2) XP_020908555.1 E3 ubiquitin-protein ligase RING2 [EPAL Cnidaria]
