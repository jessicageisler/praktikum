#!/bin/bash

#Purpose: Filters blastp results, first selecting matches with evalue < 0.00001, then sorting the matches by corresponding evalue (ascending)
#Input: blastp result table; one table per protein and species, respectively (generated using 'script 2_dbquery')
#Output: filtered and sorted blastp result table ; one table per protein and species, respectively 
#script must be run from the folder containing all the input files (individual blastp result files)
#input directory contains blast tables, one per species-query protein analysis (protein_species_tab.txt): ./Proteome/allDB/blast
#output directory: ./Proteome/allDB/blast/summary



for file in *_tab.txt
	 do

# takes filename and removes the '.txt' suffix
		nvar=$( echo $file | sed -e  's/.txt//g' )

# selects matches with evalue < 0.00001, then sorts the filtered matches by corresponding evalue (ascending)
 		awk '$11 < 0.00001 {print}' $file > ./summary/${nvar}.f.txt  | sort -t$'\t' -k11g
	done

