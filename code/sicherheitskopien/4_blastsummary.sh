#!/bin/bash

#Purpose: concatenates the 5 best matches (by evalue) for each species in one file per query protein, i.e. for each query protein the file contains the best matches for each species
#Input: blastp results table filtered for matches with evalue < 0.00001 and sorted by ascending evalue (generated using 'script 3_blastfilter')
#Output: one file per query protein containing the 5 best matches (if significant matches exist)
#script must be run from the folder containing all the input files (individual filtered and sorted blastp result files) (input directory)
#input directory: ./Proteome/allDB/blast/summary
#output directory: ./Proteome/allDB/blast/summary/comb


for file in *_tab.f.txt 
          do
#removes first the suffix '_tab.f.txt' of the filename and then the prefix containing the query proteins' name; 
#only the species/db name remains
		dbx=$( echo $file | sed -e  's/_tab.f.txt//g' )
		db=$( echo $dbx | sed -e  's/.*_//g' )

#identification of the query protein and writing the 5 matches with lowest evalue to the file in which all best matches are concatenated (1 summary file per query protein)		
		if [[ $file == PHC1seq_* ]];  then  awk  'NR <=5 {print dbname"\t"$0 }'  dbname="$db" $file >> ./comb/PHC1.txt
                elif [[ $file == PHC2seq_* ]];  then  awk  'NR <=5 {print dbname"\t"$0 }' dbname="$db" $file >> ./comb/PHC2.txt
                elif [[ $file == PHC3seq_* ]];  then  awk  'NR <=5 {print dbname"\t"$0 }'  dbname="$db" $file >> ./comb/PHC3.txt
                elif [[ $file == RING1seq_* ]];  then  awk  'NR <=5 {print dbname"\t"$0 }'  dbname="$db" $file >> ./comb/RING1.txt
                elif [[ $file == RING2seq_* ]];  then  awk  'NR <=5 {print dbname"\t"$0 }'  dbname="$db" $file >> ./comb/RING2.txt 
                elif [[ $file == RING2isoformseq_* ]];  then  awk  'NR <=5 {print dbname"\t"$0 }' dbname="$db" $file >> ./comb/RING2iso.txt  
                elif [[ $file == SCMH1seq_* ]];  then  awk  'NR <=5 {print dbname"\t"$0 }' dbname="$db" $file >> ./comb/SCMH1.txt 
		else :
                fi
         done
