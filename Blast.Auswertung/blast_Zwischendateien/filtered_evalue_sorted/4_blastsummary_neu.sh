#!/bin/bash

#Purpose: concatenates the 5 best matches (by evalue) for each species in one file per query protein, i.e. for each query protein the file contains the best matches for each species
#Input: blastp results table filtered for matches with evalue < 0.00001 and sorted by ascending evalue (generated using 'script 3_blastfilter')
#Output: one file per query protein containing the 5 best matches (if significant matches exist)
#script must be run from the folder containing all the input files (individual filtered and sorted blastp result files) (input directory)
#input directory: ./Proteome/allDB/blast/summary
#output directory: ./Proteome/allDB/blast/summary/comb


for file in *_tab.f.txt 
          do
#identification of the query protein and writing the 5 matches with lowest evalue to the file in which all best matches are concatenated (1 summary file per query protein)		
		if [[ $file == PHC1seq_* ]];  then  awk  'NR <=3 {print $0 }'  $file >> ./comb/PHC1.txt
                elif [[ $file == PHC2seq_* ]];  then  awk  'NR <=3 {print $0 }' $file >> ./comb/PHC2.txt
                elif [[ $file == PHC3seq_* ]];  then  awk  'NR <=3 {print $0 }'  $file >> ./comb/PHC3.txt
                elif [[ $file == RING1seq_* ]];  then  awk  'NR <=3 {print $0 }' $file >> ./comb/RING1.txt
                elif [[ $file == RING2seq_* ]];  then  awk  'NR <=3 {print $0 }' $file >> ./comb/RING2.txt 
                elif [[ $file == RING2isoformseq_* ]];  then  awk  'NR <=3 {print $0 }' $file >> ./comb/RING2iso.txt  
                elif [[ $file == SCMH1seq_* ]];  then  awk  'NR <=3 {print $0 }' $file >> ./comb/SCMH1.txt 
		else :
                fi
         done
