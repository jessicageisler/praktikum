#!/bin/sh

#Purpose: performs blastp of each query protein against each species provided in proteinlist.txt and dbllist.txt, respectively
#Input 1: List of query proteins providing the location of the query protein fasta file
#Input 2: List of species (termed 'db') providing the location of each species' fasta file which contain the species' proteins
#Output:  blastp results in table format (one file per query protein and species, respectively)
#Output name: proteinName_speciesAbbreviation_tab.txt
#input directory contains db files (.fa, .phr, .pin, .psq) and query protein sequences (.fa) : ./Proteome/allDB
#output directory: ./Proteome/allDB/blast


# Looping through list of query proteins
while read protein
	do 

#Looping through list of species ('db')
	while read db
		do
# performs the blast analysis, comparing aminoacid sequences (blastp)
		blastp -db $db -query $protein -outfmt 6 > ./blast/${protein}_${db}_tab.txt
		done < ./dblist.txt
	
	blastp -db $db -query $protein -outfmt 6 > ./blast/${protein}_${db}_tab.txt
	done < ./proteinlist.txt
