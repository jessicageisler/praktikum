#!/bin/bash

#Purpose: assigns to each blast match the species name and family 
#Input1: list with 3 columns containing the abbreviation of the species name, the full species name and the species' family ('family_species_list')
#Input 2: summary file with the best matches for a query protein in each species/db (generated using script '4_blastsummary') 
#Output: joined table with header (containg information on species and blast scoring values for each match)
#script must be run from the folder containing all the input files (species information list and combined blastp results table)
#input directory: ./Proteome/allDB/blast/summary/comb
#output directory: ./Proteome/allDB/blast/summary/comb


for file in *.txt
	 do
		nvar=$( echo $file | sed -e  's/.txt//g' )

# joining the list with the species information (input 1) with the file of concatenated blastp results (input 2)
# -a 1: left join
# -e NA: in case corresponding data is missing in input 2, write 'NA'
# -t $'\t': tab-separted columns 
# -1 1 -2 -1: joining value is the species' name abbreviation, which in both input files is found in $1

		join -a 1 -e NA -t $'\t' -1 1 -2 1 -o '1.3,0,1.2,2.2,2.3,2.12,2.13,2.4,2.5,2.6,2.7,2.8,2.9,2.10,2.11' <(sort -k1 family_species_list) <(sort -k1 $file) > ./${nvar}.family.woHead.txt  

	done

# sort joined data by species family name ($1), if identical by the species' protein name ($5), if identical by the blast matches evalue ($6)

sort -t$'\t' -k1,1 -k5,5 -k6,6g ./PHC1.family.woHead.txt   > ./PHC1.family.woHead.sort.txt
sort -t$'\t' -k1,1 -k5,5 -k6,6g ./PHC2.family.woHead.txt   > ./PHC2.family.woHead.sort.txt
sort -t$'\t' -k1,1 -k5,5 -k6,6g ./PHC3.family.woHead.txt   > ./PHC3.family.woHead.sort.txt
sort -t$'\t' -k1,1 -k5,5 -k6,6g ./RING1.family.woHead.txt   > ./RING1.family.woHead.sort.txt
sort -t$'\t' -k1,1 -k5,5 -k6,6g ./RING2.family.woHead.txt   > ./RING2.family.woHead.sort.txt
sort -t$'\t' -k1,1 -k5,5 -k6,6g ./RING2iso.family.woHead.txt   > ./RING2iso.family.woHead.sort.txt
sort -t$'\t' -k1,1 -k5,5 -k6,6g ./SCMH1.family.woHead.txt   > ./SCMH1.family.woHead.sort.txt

# creates header and concatenates it with the joined table
echo -e "family \t species_abbrev \t species_name \t qseqid \t sseqid \t evalue \t bitscore \t pident \t length \t mismatch \t gapopen \t qstart \t qend \t sstart \t send" | cat - ./PHC1.family.woHead.sort.txt > ./PHC1.family.txt
echo -e "family \t species_abbrev \t species_name \t qseqid \t sseqid \t evalue \t bitscore \t pident \t length \t mismatch \t gapopen \t qstart \t qend \t sstart \t send" | cat - ./PHC2.family.woHead.sort.txt > ./PHC2.family.txt
echo -e "family \t species_abbrev \t species_name \t qseqid \t sseqid \t evalue \t bitscore \t pident \t length \t mismatch \t gapopen \t qstart \t qend \t sstart \t send" | cat - ./PHC3.family.woHead.sort.txt > ./PHC3.family.txt 
echo -e "family \t species_abbrev \t species_name \t qseqid \t sseqid \t evalue \t bitscore \t pident \t length \t mismatch \t gapopen \t qstart \t qend \t sstart \t send" | cat - ./RING1.family.woHead.sort.txt > ./RING1.family.txt 
echo -e "family \t species_abbrev \t species_name \t qseqid \t sseqid \t evalue \t bitscore \t pident \t length \t mismatch \t gapopen \t qstart \t qend \t sstart \t send" | cat - ./RING2.family.woHead.sort.txt > ./RING2.family.txt 
echo -e "family \t species_abbrev \t species_name \t qseqid \t sseqid \t evalue \t bitscore \t pident \t length \t mismatch \t gapopen \t qstart \t qend \t sstart \t send" | cat - ./RING2iso.family.woHead.sort.txt > ./RING2iso.family.txt
echo -e "family \t species_abbrev \t species_name \t qseqid \t sseqid \t evalue \t bitscore \t pident \t length \t mismatch \t gapopen \t qstart \t qend \t sstart \t send" | cat - ./SCMH1.family.woHead.sort.txt > ./SCMH1.family.txt

#removes temporary files (before sorting and without header)
rm  ./PHC1.txt ./PHC2.txt ./PHC3.txt  ./RING1.txt  ./RING2.txt ./RING2iso.txt ./SCMH1.txt
rm  ./PHC1.family.woHead.txt ./PHC2.family.woHead.txt ./PHC3.family.woHead.txt  ./RING1.family.woHead.txt  ./RING2.family.woHead.txt ./RING2iso.family.woHead.txt ./SCMH1.family.woHead.txt
rm  ./PHC1.family.woHead.sort.txt  ./PHC2.family.woHead.sort.txt ./PHC3.family.woHead.sort.txt ./RING1.family.woHead.sort.txt ./RING2.family.woHead.sort.txt ./RING2iso.family.woHead.sort.txt ./SCMH1.family.woHead.sort.txt
