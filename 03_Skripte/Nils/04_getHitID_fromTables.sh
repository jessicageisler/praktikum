# Filtert aus Blast-Ergebnissen die "besten" Hits mit kleinestem Evalue.
# CutOff = 1*10^-10 (0.00000000001)
# Ausgabe der Query-ID => um die dazugehörigen Sequenzen aus den Datenbanken zu extrahieren

cd ./blast/CUSTOM/
for f in PHC1seq_*_custom.txt; do cat ${f} | awk '$3 < 0.00000000001 {print}' | sort -k3g | head -1l | cut -f2 >> ./All_Hits/Hits_SeqID_PHC1seq ; done;
for f in PHC2seq_*_custom.txt; do cat ${f} | awk '$3 < 0.00000000001 {print}' | sort -k3g |  head -1l | cut -f2  >> ./All_Hits/Hits_SeqID_PHC2seq ; done;
for f in PHC3seq_*_custom.txt; do cat ${f} | awk '$3 < 0.00000000001 {print}' | sort -k3g |  head -1l | cut -f2  >> ./All_Hits/Hits_SeqID_PHC3seq ; done;
for f in RING1seq_*_custom.txt; do cat ${f} | awk '$3 < 0.00000000001 {print}' | sort -k3g |  head -1l | cut -f2 >> ./All_Hits/Hits_SeqID_RING1seq  ; done;
for f in RING2isoformseq_*_custom.txt; do cat ${f} | awk '$3 < 0.00000000001 {print}' | sort -k3g |  head -1l | cut -f2 >> ./All_Hits/Hits_SeqID_RING2isoformseq; done;
for f in RING2seq_*_custom.txt; do cat ${f} | awk '$3 < 0.00000000001 {print}' | sort -k3g |  head -1l | cut -f2   >> ./All_Hits/Hits_SeqID_RING2seq; done;
for f in SCMH1seq_*_custom.txt; do cat ${f} | awk '$3 < 0.00000000001 {print}' | sort -k3g |  head -1l | cut -f2   >> ./All_Hits/Hits_SeqID_SCMH1seq; done;
for f in SCML2isoformseq_*_custom.txt; do cat ${f} | awk '$3 < 0.00000000001 {print}' | sort -k3g |  head -1l | cut -f2 >> ./All_Hits/Hits_SeqID_SCML2isoformseq; done;
for f in SCML2seq_*_custom.txt; do cat ${f} | awk '$3 < 0.00000000001 {print}' | sort -k3g |  head -1l | cut -f2 >> ./All_Hits/Hits_SeqID_SCML2seq; done;
