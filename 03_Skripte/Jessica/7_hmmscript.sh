#!/bin/sh

while read hmm
	do 

	while read db
		do

		hmmsearch --tblout ./HMM/hmm_${hmm}_${db}.faa -E 1e-5 $hmm $db
		done < ./dblist.txt
	
	hmmsearch --tblout ./HMM/hmm_${hmm}_${db}.faa -E 1e-5 $hmm $db
	done < ./hmmlist.txt
