#!/bin/bash

#Purpose: Filters blastp results, first selecting matches with evalue < 0.00001, then sorting the matches by corresponding evalue (ascending)
#Input: blastp result table; one table per protein and species, respectively (generated using 'script 2_dbquery')
#Output: filtered and sorted blastp result table ; one table per protein and species, respectively 
#script must be run from the folder containing all the input files (individual blastp result files)
#input directory contains blast tables, one per species-query protein analysis (protein_species_tab.txt): ./Proteome/allDB/blast
#output directory: ./Proteome/allDB/blast/summary

#column order of blast output: dbname sseqid evalue qcovs score bitscore pident length mismatch gapopen qstart qend sstart send

for file in *_tab.txt
	 do

# takes filename and removes the '.txt' suffix
		nvar=$( echo $file | sed -e  's/.txt//g' )

# selects matches with evalue < 1x10^-10
 		awk '$3 < 0.0000000001 {print}' $file > ./filtered_evalue_sorted/${nvar}.f.temp.txt
		awk '$3 < 0.0000000001 {print}' $file > ./filtered_score_sorted/${nvar}.f.temp.txt
#sort by evalue or by bitscore
		sort -t$'\t' -k3,3g ./filtered_evalue_sorted/${nvar}.f.temp.txt > ./filtered_evalue_sorted/${nvar}.f.txt
		sort -t$'\t' -k6,6gr ./filtered_score_sorted/${nvar}.f.temp.txt > ./filtered_score_sorted/${nvar}.f.txt 

		rm ./filtered_evalue_sorted/${nvar}.f.temp.txt
		rm ./filtered_score_sorted/${nvar}.f.temp.txt

	done



 #		awk '$3 < 0.0000000001 {print}' $file > ./filtered_evalue_sorted/${nvar}.f.txt  | sort -t$'\t' -k3g
#		awk '$3 < 0.0000000001 {print}' $file > ./filtered_score_sorted/${nvar}.f.txt  | sort -t$'\t' -k5gr
