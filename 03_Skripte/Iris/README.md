# How to use proteome download script

cd to "praktikum"

`python3 proteomdownload/proteomdownload.py Proteome`

`cd Proteome`

`for file in */*.gz; do gunzip $file; done`

`cd Porifera`

`for file in *.zip; do unzip $file; done`

`rm *.zip`
