# Filtert aus Blast-Ergebnissen die "besten" Hits mit kleinestem Evalue.
# CutOff = 1*10^-10 (0.00000000001)
# Ausgabe der TAXA + QUERY-ID + E-VALUE


cd ./blast/CUSTOM/
for f in PHC1seq_*_custom.txt; do cat ${f} | awk '$3 < 0.00000000001 {print}' | sort -k3g | head -1l | cut -f1,2,3 >> ./All_Hits/Hit_Taxa+ID+Value_PHC1 ; done;
for f in PHC2seq_*_custom.txt; do cat ${f} | awk '$3 < 0.00000000001 {print}' | sort -k3g |  head -1l | cut -f1,2,3  >> ./All_Hits/Hit_Taxa+ID+Value_PHC2 ; done;
for f in PHC3seq_*_custom.txt; do cat ${f} | awk '$3 < 0.00000000001 {print}' | sort -k3g |  head -1l | cut -f1,2,3  >> ./All_Hits/Hit_Taxa+ID+Value_PHC3 ; done;
for f in RING1seq_*_custom.txt; do cat ${f} | awk '$3 < 0.00000000001 {print}' | sort -k3g |  head -1l | cut -f1,2,3 >> ./All_Hits/Hit_Taxa+ID+Value_RING1 ; done;
for f in RING2isoformseq_*_custom.txt; do cat ${f} | awk '$3 < 0.00000000001 {print}' | sort -k3g |  head -1l | cut -f1,2,3 >> ./All_Hits/Hit_Taxa+ID+Value_RING2isoform; done;
for f in RING2seq_*_custom.txt; do cat ${f} | awk '$3 < 0.00000000001 {print}' | sort -k3g |  head -1l | cut -f1,2,3   >> ./All_Hits/Hit_Taxa+ID+Value_RING2; done;
for f in SCMH1seq_*_custom.txt; do cat ${f} | awk '$3 < 0.00000000001 {print}' | sort -k3g |  head -1l | cut -f1,2,3   >> ./All_Hits/Hit_Taxa+ID+Value_SCMH1; done;
for f in SCML2isoformseq_*_custom.txt; do cat ${f} | awk '$3 < 0.00000000001 {print}' | sort -k3g |  head -1l | cut -f1,2,3 >> ./All_Hits/Hit_Taxa+ID+Value_SCML2isoform; done;
for f in SCML2seq_*_custom.txt; do cat ${f} | awk '$3 < 0.00000000001 {print}' | sort -k3g |  head -1l | cut -f1,2,3 >> ./All_Hits/Hit_Taxa+ID+Value_SCML2; done;
