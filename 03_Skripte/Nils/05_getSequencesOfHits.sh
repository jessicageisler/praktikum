# Ausführung von fastacmd
# Ausgabe Sammlung von Proteinsequenzen als .multifasta
# Benötigt werden als Input:
# 1) Proteom-Datenbanken (02_CreateBlastDBs.sh)
# 2) Listen mit Query-IDs (04_getHitID_fromTables.sh)
# ! Empfehlenswert, wäre es die Datenbanken mit einer Dateiendung ".db" zu versehen,
# um die auftretenden Fehlermeldungen zu beheben. 
# (Für mich stellte das leider ein Problem dar, da ich den "Dateinamen" der Datenbank
# direkt genutzt habe um die Taxa zuzuordnen.)

for file in ./all_Proteoms/*
do
fastacmd -d $file -i ~/Dokumente/bioinformatik/praktikum/blast/CUSTOM/All_Hits/Hits_SeqID_PHC1seq >> ./SequencesOfHits/PHC1.multifasta;
fastacmd -d $file -i ~/Dokumente/bioinformatik/praktikum/blast/CUSTOM/All_Hits/Hits_SeqID_PHC2seq >> ./SequencesOfHits/PHC2.multifasta;
fastacmd -d $file -i ~/Dokumente/bioinformatik/praktikum/blast/CUSTOM/All_Hits/Hits_SeqID_PHC3seq >> ./SequencesOfHits/PHC3.multifasta;
fastacmd -d $file -i ~/Dokumente/bioinformatik/praktikum/blast/CUSTOM/All_Hits/Hits_SeqID_RING1seq >> ./SequencesOfHits/RING1.multifasta;
fastacmd -d $file -i ~/Dokumente/bioinformatik/praktikum/blast/CUSTOM/All_Hits/Hits_SeqID_RING2isoformseq >> ./SequencesOfHits/RING2isoform.multifasta;
fastacmd -d $file -i ~/Dokumente/bioinformatik/praktikum/blast/CUSTOM/All_Hits/Hits_SeqID_RING2seq >> ./SequencesOfHits/RING2.multifasta;
fastacmd -d $file -i ~/Dokumente/bioinformatik/praktikum/blast/CUSTOM/All_Hits/Hits_SeqID_SCMH1seq >> ./SequencesOfHits/SCMH1.multifasta;
fastacmd -d $file -i ~/Dokumente/bioinformatik/praktikum/blast/CUSTOM/All_Hits/Hits_SeqID_SCML2isoformseq_ >> ./SequencesOfHits/SCML2isoform.multifasta;
fastacmd -d $file -i ~/Dokumente/bioinformatik/praktikum/blast/CUSTOM/All_Hits/Hits_SeqID_SCML2seq >> ./SequencesOfHits/SCML2.multifasta;
echo "done"

done
